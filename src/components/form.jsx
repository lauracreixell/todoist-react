import { useState } from "react";
import { v4 as uuid } from 'uuid';

import '../styles/home.scss';

function NewTask() {
    const [todos, setTodos] = useState([]);
    // const [completed, setCompletes] = useState([]);
    const [form, setForm] = useState({
        task: "",
        type: "",
        id: "",
    });

    const handleSubmit = (event) => {
        event.preventDefault();

        setTodos([...todos, form]);

        setForm({
            task: "",
            type: "",
            id: uuid(),
        });
    };
    //console.log(todos);

    const handleChangeInput = (event) => {
        const newValue = event.target.value;
        const inputName = event.target.name;

        setForm({
            ...form,
            [inputName]: newValue,
        });
    }

    return (
        <form className="form" onSubmit={handleSubmit}>
            <div className="form__container">
                <label htmlFor="task"></label>
                <input className="form__container__input" id="task" name="task" type="text" value={form.task} onChange={handleChangeInput} placeholder="Tarea" />

                <label htmlFor="type"></label>
                <input className="form__container__input" id="type" name="type" type="text" value={form.type} onChange={handleChangeInput} placeholder="Tipo" />

            </div>

            <div className="form__buttons">
                <button type="submit">Añadir</button>
                <button>Cancelar</button>
            </div>

        </form>
    );
}

export default NewTask;