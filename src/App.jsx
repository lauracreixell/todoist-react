import { useState } from "react";
import { Switch, Route } from 'react-router-dom';

import Home from './pages/home';
import Completed from './pages/completed'

import './App.scss';

function App() {


  return (
    <div className="App">


      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>

        <Route path="/completed" exact>
          <Completed />
        </Route>

      </Switch>
    </div>
  );
}

export default App;
